import React from "react";
import MainFolder from "../components/MainFolder";

function App() {
  return (
    <React.Fragment>
        <MainFolder/>
    </React.Fragment>
  );
}

export default App;
