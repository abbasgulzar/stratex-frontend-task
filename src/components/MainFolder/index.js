import React from "react";
import { EMPLOYEES } from "../../constants";

const MainFolder = () => {
  const [employees, setEmployees] = React.useState([]);
  const [searchValue, setSearchValue] = React.useState("");
  const [firstName, setFirstName] = React.useState("");
  const [lastName, setLastName] = React.useState("");
  const [role, setRole] = React.useState(1);

  const [modal, showModal] = React.useState(false);

  React.useEffect(() => {
    setEmployees(EMPLOYEES);
  }, []);

  return (
    <React.Fragment>
      <nav className="navbar sticky-top navbar-light bg-light">
        <div className="container-fluid w-50">
          <label className="col-sm-2 col-form-label">
            Search for employee:
          </label>
          <div className="col-sm-10">
            <input
              onChange={(event) => {
                setSearchValue(event.target.value);
                setEmployees((prev) => {
                  return EMPLOYEES.filter(
                    (e) =>
                      e.firstName
                        .toLowerCase()
                        .includes(event.target.value.toLowerCase()) ||
                      e.lastName
                        .toLowerCase()
                        .includes(event.target.value.toLowerCase())
                  );
                });
              }}
              //   onKeyDown={(event) => {
              //     if (event.key === 8) {
              //       setEmployees((prev) => {
              //         return prev.filter(
              //           (e) =>
              //             e.firstName
              //               .toLowerCase()
              //               .includes(event.target.value.toLowerCase()) ||
              //             e.lastName
              //               .toLowerCase()
              //               .includes(event.target.value.toLowerCase())
              //         );
              //       });
              //     }
              //   }}
              value={searchValue}
              type="text"
              className="form-control"
              id="search-user"
              placeholder="Enter a name"
            />
          </div>
        </div>
      </nav>

      {/* part for displaying user content */}
      <div className="container pt-5">
        <h2>Users</h2>
        <table className="table mb-5 align-middle" id="users">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">First</th>
              <th scope="col">Last</th>
              <th scope="col">Role</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            {employees
              .filter((employee) => employee.role === 1)
              .map((employee, index) => (
                <tr key={employee.id}>
                  <th scope="row">{index + 1}</th>
                  <td style={{ maxWidth: 10 }}>{employee.firstName}</td>
                  <td style={{ maxWidth: 10 }}>{employee.lastName}</td>
                  <td style={{ maxWidth: 10 }}>User</td>
                  <td style={{ maxWidth: 10 }}>
                    <button
                      onClick={() => {
                        setEmployees((prev) => {
                          return prev.filter((e) => e.id !== employee.id);
                        });
                      }}
                      type="button"
                      className="btn btn-outline-danger btn-sm"
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        <h2>Senior Users</h2>
        <table className="table mb-5 align-middle" id="senior-users">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">First</th>
              <th scope="col">Last</th>
              <th scope="col">Role</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            {employees
              .filter((employee) => employee.role === 2)
              .map((employee, index) => (
                <tr key={employee.id}>
                  <th scope="row">{index + 1}</th>
                  <td style={{ maxWidth: 10 }}>{employee.firstName}</td>
                  <td style={{ maxWidth: 10 }}>{employee.lastName}</td>
                  <td style={{ maxWidth: 10 }}>Senior User</td>
                  <td style={{ maxWidth: 10 }}>
                    <button
                      onClick={() => {
                        setEmployees((prev) => {
                          return prev.filter((e) => e.id !== employee.id);
                        });
                      }}
                      type="button"
                      className="btn btn-outline-danger btn-sm"
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        <h2>WFM</h2>
        <table className="table mb-5 align-middle" id="wfm-users">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">First</th>
              <th scope="col">Last</th>
              <th scope="col">Role</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            {employees
              .filter((employee) => employee.role === 3)
              .map((employee, index) => (
                <tr key={employee.id}>
                  <th scope="row">{index + 1}</th>
                  <td style={{ maxWidth: 10 }}>{employee.firstName}</td>
                  <td style={{ maxWidth: 10 }}>{employee.lastName}</td>
                  <td style={{ maxWidth: 10 }}>WFM Professional</td>
                  <td style={{ maxWidth: 10 }}>
                    <button
                      onClick={() => {
                        setEmployees((prev) => {
                          return prev.filter((e) => e.id !== employee.id);
                        });
                      }}
                      type="button"
                      className="btn btn-outline-danger btn-sm"
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        <button
          type="button"
          className="btn btn-primary"
          onClick={() => showModal(true)}
        >
          Add User
        </button>
      </div>

      {/* end of displaying content */}

      {/* modal area */}

      {modal && (
        <div class="modal is-active">
          <div class="modal-background"></div>
          <div class="modal-card">
            <header class="modal-card-head">
              <p class="modal-card-title">Modal title</p>
              <button class="delete" aria-label="close"></button>
            </header>
            <section class="modal-card-body">
              <div className="modal-body">
                <div className="mb-3">
                  <label>First Name</label>
                  <input
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                    type="text"
                    className="form-control"
                    placeholder="Mark"
                  />
                </div>
                <div className="mb-3">
                  <label>Last Name</label>
                  <input
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                    type="text"
                    className="form-control"
                    placeholder="Otto"
                  />
                </div>
                <div className="mb-3">
                  <label>Role</label>
                  <select
                    onChange={(e) => {
                      setRole(e.target.value);
                      console.log(e.target.value);
                    }}
                    className="form-select"
                    value={role}
                  >
                    <option value="1">User</option>
                    <option value="2">Senior User</option>
                    <option value="3">WFM</option>
                  </select>
                </div>
              </div>
            </section>
            <footer class="modal-card-foot">
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  onClick={() => showModal(false)}
                >
                  Close
                </button>
                <button
                  onClick={() => {
                    setEmployees((prev) => {
                      const data = [...prev];
                      console.log("prev", role);
                      data.push({
                        id: prev.length + 1,
                        firstName,
                        lastName,
                        role,
                      });
                      console.log("new", data);
                      return [...data];
                    });
                    showModal(false);
                  }}
                  type="button"
                  className="btn btn-primary"
                >
                  Save changes
                </button>
              </div>
            </footer>
          </div>
        </div>
      )}

      {/* end of modal area */}
    </React.Fragment>
  );
};

export default MainFolder;
